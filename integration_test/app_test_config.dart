class AppTestConfig {
  static const String email = "test@mail.com";
  static const String password = "test1234@";
  static const String invalidEmail = "test";
  static const String invalidPassword = "pass";
  static const String badEmail = "example@mail.com";
  static const String badPassword = "password123";
  static const String firstName = "User";
  static const String lastName = "Testing";
}
