import 'package:flutter_test/flutter_test.dart';
import 'package:weather_test/main.dart';
import '../robots/login_extensions/login.dart';
import '../robots/login_page_robot.dart';

class LoginPageManager {
  final LoginPageRobot _loginPageRobot = LoginPageRobot();

  Future<void> _init(WidgetTester tester) async {
    await main();
    _loginPageRobot.tester = tester;
    await _loginPageRobot.init();
  }

  void _runWidgetTest(String description, Future Function() methodToCall) {
    testWidgets(description, (WidgetTester tester) async {
      await _init(tester);
      await methodToCall();
    });
  }

  void testScreen() {
    _login();
  }

  Future<void> _login() async {
    _runWidgetTest("Login: empty fields error", _loginPageRobot.loginKoEmailAndPasswordEmpty);
    _runWidgetTest("Login: empty email", _loginPageRobot.loginKoEmailEmpty);
    _runWidgetTest("Login: empty password", _loginPageRobot.loginKoPasswordEmpty);
    _runWidgetTest("Login: invalid email", _loginPageRobot.loginKoEmailInvalid);
    _runWidgetTest("Login: invalid password", _loginPageRobot.loginKoPasswordInvalid);
    _runWidgetTest("Login: user not exist", _loginPageRobot.loginKoEmailOrPasswordNotExist);
    _runWidgetTest("Login: login sucess", _loginPageRobot.loginOk);
  }
}
