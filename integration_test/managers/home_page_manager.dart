import 'package:flutter_test/flutter_test.dart';
import 'package:weather_test/main.dart';
import '../robots/home_extensions/logout.dart';
import '../robots/home_extensions/user_name.dart';
import '../robots/home_page_robot.dart';

class HomePageManager {
  final HomePageRobot _homePageRobot = HomePageRobot();

  Future<void> _init(WidgetTester tester) async {
    await main();
    _homePageRobot.tester = tester;
    await _homePageRobot.init();
  }

  void _runWidgetTest(String description, Future Function() methodToCall) {
    testWidgets(description, (WidgetTester tester) async {
      await _init(tester);
      await methodToCall();
    });
  }

  void testScreen() {
    _userName();
    _logout();
  }

  Future<void> _userName() async {
    _runWidgetTest("Home: Display logged user", _homePageRobot.displayUserName);
  }

  Future<void> _logout() async {
    _runWidgetTest("Home: tap logout button", _homePageRobot.logout);
    _runWidgetTest("Home: logout action", _homePageRobot.confirmLogout);
  }
}
