import 'package:flutter_test/flutter_test.dart';
import 'package:http_mock_adapter/http_mock_adapter.dart';
import 'package:weather_test/app/pages/index.dart';
import 'package:weather_test/data/apis/index.dart';
import 'package:weather_test/utils/index.dart';
import '../app_test_config.dart';
import '../mocks/weather_mock.dart';

class HomePageRobot {
  late WidgetTester tester;
  late WeatherMock weatherMock;
  late DioAdapter dioAdapterWeather;

  Future init() async {
    await HiveUtils.saveLoggedUser(
      email: AppTestConfig.email,
      firstName: AppTestConfig.firstName,
      lastName: AppTestConfig.lastName,
    );
    weatherMock = WeatherMock();
    dioAdapterWeather = DioAdapter(
      dio: WeatherApi().interceptor.dio!,
    );
    weatherMock.getWeatherDetails(
      dioAdapterWeather,
    );
  }

  // GET Finder

  Finder getLoggedUserNameFinder() {
    return find.byKey(keyFromEnumValue(HomePageKeys.userNameKey));
  }

  Finder getLogoutButtonFinder() {
    return find.byKey(keyFromEnumValue(HomePageKeys.logoutButtonKey));
  }

  Finder getConfirmLogoutButtonFinder() {
    return find.byKey(keyFromEnumValue(HomePageKeys.confirmLogoutButtonKey));
  }

  // TAP

  Future tapLogoutButton() async {
    await tester.pumpAndSettle();
    await tester.tap(getLogoutButtonFinder());
    await tester.pumpAndSettle();
  }

  Future tapConfirmLogoutButton() async {
    await tester.tap(getConfirmLogoutButtonFinder());
    await tester.pumpAndSettle();
  }
}
