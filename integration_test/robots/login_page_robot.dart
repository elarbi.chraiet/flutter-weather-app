import 'package:flutter_test/flutter_test.dart';
import 'package:weather_test/app/pages/index.dart';
import 'package:weather_test/utils/index.dart';
import 'package:weather_test/widgets/index.dart';

class LoginPageRobot {
  late WidgetTester tester;

  Future init() async {
    await HiveUtils.clearLoggedUserBox();
  }

  // FILL
  Future fillEmailTextInput(String txt) async {
    await tester.pumpAndSettle();
    await tester.enterText(getEmailFinder(), txt);
    await tester.pumpAndSettle();
  }

  Future fillPasswordTextInput(String password) async {
    await tester.pumpAndSettle();
    await tester.enterText(getPasswordFinder(), password);
    await tester.pumpAndSettle();
  }

  // GET Finder

  Finder getEmailFinder() {
    return find.byKey(keyFromEnumValue(LoginPageKeys.emailInputKey));
  }

  Finder getPasswordFinder() {
    return find.byKey(keyFromEnumValue(LoginPageKeys.passwordInputKey));
  }

  Finder getLoginButtonFinder() {
    return find.byKey(keyFromEnumValue(LoginPageKeys.loginButtonKey));
  }

  // GET Widgets

  ElevatedButtonWidget getLoginButton() {
    return getLoginButtonFinder().evaluate().first.widget as ElevatedButtonWidget;
  }

  InputTextField getEmailTextInput() {
    return getPasswordFinder().evaluate().first.widget as InputTextField;
  }

  InputTextField getPasswordTextInput() {
    return getPasswordFinder().evaluate().first.widget as InputTextField;
  }

  // TAP

  Future tapLoginButton() async {
    await tester.tap(getLoginButtonFinder());
    await tester.pumpAndSettle();
  }
}
