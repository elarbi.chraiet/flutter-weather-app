import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:weather_test/app/config/constants/index.dart';
import 'package:weather_test/app/pages/index.dart';
import '../home_page_robot.dart';

extension HomePageRobotLogout on HomePageRobot {
  Future logout() async {
    await tapLogoutButton();
    expect(
        find.byWidgetPredicate(
          (widget) => widget is Text && widget.data!.contains(AppStrings.logoutDescriptionText),
        ),
        findsOneWidget);
  }

  Future confirmLogout() async {
    await tapLogoutButton();
    await tapConfirmLogoutButton();
    expect(find.byType(LoginPage), findsOneWidget);
  }
}
