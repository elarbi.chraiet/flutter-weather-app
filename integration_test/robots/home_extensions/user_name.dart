import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import '../../app_test_config.dart';
import '../home_page_robot.dart';

extension HomePageRobotLogout on HomePageRobot {
  Future displayUserName() async {
    await tester.pumpAndSettle();
    expect(getLoggedUserNameFinder(), findsOneWidget);
    expect(
        find.byWidgetPredicate(
          (widget) =>
              widget is Text &&
              widget.data!.contains(
                '${AppTestConfig.firstName} ${AppTestConfig.lastName}',
              ),
        ),
        findsOneWidget);
  }
}
