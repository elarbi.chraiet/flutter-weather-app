import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:weather_test/app/config/constants/app_strings.dart';
import 'package:weather_test/app/pages/home_page.dart';

import '../../app_test_config.dart';
import '../login_page_robot.dart';

extension LoginPageRobotLogin on LoginPageRobot {
  Future loginKoEmailAndPasswordEmpty() async {
    await fillEmailTextInput('');
    await fillPasswordTextInput('');
    await tapLoginButton();
    expect(
        find.byWidgetPredicate((widget) => widget is Text && widget.data!.contains(AppStrings.requiredEmailErrorText)),
        findsOneWidget);
    expect(
        find.byWidgetPredicate(
            (widget) => widget is Text && widget.data!.contains(AppStrings.requiredPasswordErrorText)),
        findsOneWidget);
  }

  Future loginKoEmailEmpty() async {
    await fillPasswordTextInput(AppTestConfig.password);
    await tapLoginButton();
    expect(
        find.byWidgetPredicate((widget) => widget is Text && widget.data!.contains(AppStrings.requiredEmailErrorText)),
        findsOneWidget);
  }

  Future loginKoPasswordEmpty() async {
    await fillEmailTextInput(AppTestConfig.email);
    await tapLoginButton();
    expect(
        find.byWidgetPredicate(
            (widget) => widget is Text && widget.data!.contains(AppStrings.requiredPasswordErrorText)),
        findsOneWidget);
  }

  Future loginKoEmailInvalid() async {
    await fillEmailTextInput(AppTestConfig.invalidEmail);
    await tapLoginButton();
    expect(
        find.byWidgetPredicate((widget) => widget is Text && widget.data!.contains(AppStrings.invalidEmailErrorText)),
        findsOneWidget);
  }

  Future loginKoPasswordInvalid() async {
    await fillPasswordTextInput(AppTestConfig.invalidPassword);
    await tapLoginButton();
    expect(
        find.byWidgetPredicate(
            (widget) => widget is Text && widget.data!.contains(AppStrings.invalidPasswordErrorText)),
        findsOneWidget);
  }

  Future loginKoEmailOrPasswordNotExist() async {
    await fillEmailTextInput(AppTestConfig.badEmail);
    await fillPasswordTextInput(AppTestConfig.badPassword);
    await tapLoginButton();
    expect(
        find.byWidgetPredicate((widget) => widget is Text && widget.data!.contains(AppStrings.loginErrorMessageText)),
        findsOneWidget);
  }

  Future loginOk() async {
    await fillEmailTextInput(AppTestConfig.email);
    await fillPasswordTextInput(AppTestConfig.password);
    await tapLoginButton();
    expect(find.byType(HomePage), findsOneWidget);
  }
}
