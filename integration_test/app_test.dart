import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';

import 'managers/home_page_manager.dart';
import 'managers/login_page_manager.dart';

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  group(
    'LoginPage',
    () {
      LoginPageManager().testScreen();
    },
  );

  group(
    'HomePage',
    () {
      HomePageManager().testScreen();
    },
  );
}
