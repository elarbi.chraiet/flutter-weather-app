# Flutter Weather app

weather app with open weather API. Fetch Paris weather data using location coordinates(longitude and latitude)

## Platform

ios ✔️

android ✔️

## Features & screens

### Light mode

![LightModeImage](documentation/light_mode.png)

### Dark mode

![DarkModeImage](documentation/dark_mode.png)

## Testing params

Fake login process with those params :

```
 static const String email = 'test@mail.com';
 static const String password = 'test1234@';

```

## Libraries & Tools Used

- [Visual Studio Code](https://code.visualstudio.com) (code editor )
- Flutter state managment(using flutter components)
- [Dio](https://github.com/flutterchina/dio) (for calling api)
- [Hive](https://pub.dev/packages/hive) (for locale storage)
- [Modular](https://pub.dev/packages/flutter_modular) Modular (for routing and dependency injection)
- [integration_test](https://docs.flutter.dev/testing/integration-tests) (to implement integration test)
- [http_mock_adapter](https://pub.dev/packages/http_mock_adapter) (to mock weather api used for integration test)
- Dark Theme Support

## CI

gitlab Ci edited but not working

## launch app

launch.json file content

```
{
"version": "0.2.0",
"configurations": [
{
"name": "Debug_weather",
"request": "launch",
"type": "dart",
"flutterMode": "debug",
},
{
"name": "Profile_weather",
"request": "launch",
"type": "dart",
"flutterMode": "profile",
},
{
"name": "Release_weather",
"request": "launch",
"type": "dart",
"flutterMode": "release",
},
{
"name": "Integration Test: Run Test",
"request": "launch",
"type": "dart",
"program": "./integration_test/app_test.dart",
},
]
}

```

## Development Setup

Clone the repository and run the following commands:

```

flutter pub get
flutter run

```
