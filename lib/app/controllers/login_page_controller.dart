import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:weather_test/app/config/constants/app_constants.dart';
import 'package:weather_test/utils/index.dart';

class LoginPageController extends Disposable {
  // TextEditingControllers
  final TextEditingController emailTextEditingController = TextEditingController();
  final TextEditingController passwordTextEditingController = TextEditingController();

  // FocusNodes
  final FocusNode emailFocusNode = FocusNode();
  final FocusNode passwordFocusNode = FocusNode();

  final ValueNotifier<bool?> isLoading = ValueNotifier<bool?>(false);

  void enableLoading() {
    isLoading.value = true;
  }

  void disableLoading() {
    isLoading.value = false;
  }

  // Fake login
  Future<bool?> login() async {
    enableLoading();
    // waiting 2 secondes just for displaying loader
    await Future.delayed(const Duration(seconds: 1));
    disableLoading();
    if (emailTextEditingController.text == AppConstants.email &&
        passwordTextEditingController.text == AppConstants.password) {
      HiveUtils.saveLoggedUser(
        email: AppConstants.email,
        firstName: AppConstants.firstName,
        lastName: AppConstants.lastName,
      );
      _clearFields();
      return true;
    } else {
      return false;
    }
  }

  void _clearFields() {
    emailTextEditingController.clear();
    passwordTextEditingController.clear();
  }

  @override
  void dispose() {
    emailTextEditingController.dispose();
    passwordTextEditingController.dispose();
    emailFocusNode.dispose();
    passwordFocusNode.dispose();
    isLoading.dispose();
  }
}
