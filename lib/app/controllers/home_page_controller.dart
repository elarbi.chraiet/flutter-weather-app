import 'package:flutter_modular/flutter_modular.dart';
import 'package:weather_test/data/models/index.dart';
import 'package:weather_test/data/repositories/index.dart';

class HomePageController extends Disposable {
  final WeatherRepository _weatherRepository = Modular.get<WeatherRepository>();

  Future<List<Weatherinfos>?> getWeatherDetails() async {
    var response = await _weatherRepository.findWeatherInfos();
    if (response != null && response.list != null) {
      return response.list;
    } else {
      return null;
    }
  }

  @override
  void dispose() {}
}
