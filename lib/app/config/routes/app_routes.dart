class AppRoutes {
  AppRoutes._();

  //* Login
  static const String loginPage = '/loginPage';

  //* Home
  static const String homePage = '/homePage';
}
