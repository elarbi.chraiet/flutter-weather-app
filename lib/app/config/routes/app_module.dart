import 'package:flutter_modular/flutter_modular.dart';
import 'package:weather_test/app/config/routes/routing.dart';
import 'package:weather_test/app/controllers/index.dart';
import 'package:weather_test/app/pages/index.dart';
import 'package:weather_test/data/apis/index.dart';
import 'package:weather_test/data/repositories/index.dart';
import 'package:weather_test/utils/index.dart';

class AppModule extends Module {
  // Provide a list of dependencies to inject into your project
  @override
  List<Bind> get binds => <Bind>[
        Bind((i) => LoginPageController()),
        Bind((i) => HomePageController()),
        Bind((i) => WeatherApi()),
        Bind((i) => WeatherRepository()),
      ];

  // Provide all the routes for your application
  @override
  List<ModularRoute> get routes => <ModularRoute>[
        ChildRoute(
          Modular.initialRoute,
          child: (_, __) => HiveUtils.getLoggedUserBox() != null ? const HomePage() : const LoginPage(),
          transition: TransitionType.fadeIn,
        ),
        ChildRoute(
          AppRoutes.loginPage,
          child: (_, __) => const LoginPage(),
          transition: TransitionType.fadeIn,
        ),
        ChildRoute(
          AppRoutes.homePage,
          child: (_, __) => const HomePage(),
          transition: TransitionType.fadeIn,
        ),
      ];
}
