class AppStrings {
  AppStrings._();

  static const String loginPageTitle = "S'authentifier";
  static const String emailTextFieldLabel = "Email";
  static const String passwordTextFieldLabel = "Mot de passe";
  static const String loginButtonLabel = "Se connecter";
  static const String requiredEmailErrorText = "L'email est obligatoire";
  static const String invalidEmailErrorText = "Email invalide";
  static const String requiredPasswordErrorText = "Le mot de passe est obligatoire";
  static const String invalidPasswordErrorText = "Le mot de passe doit contenir au moins 8 caractères";
  static const String loginErrorMessageText = "Email ou mot de passe invalide";
  static const String logoutLabelText = "Déconnexion";
  static const String logoutDescriptionText = "Voulez-vous déconnecter";
  static const String confirmLabelText = "Confirmer";
  static const String cancelLabelText = "Annuler";
  static const String genericErrorDescriptionText = "Une erreur est survenue.";
  static const String tryLaterText = "Réessayer plus tard";
}
