import 'package:flutter/material.dart';

class AppConstants {
  AppConstants._();

  // Fake login params
  static const String email = 'test@mail.com';
  static const String password = 'test1234@';
  static const String firstName = 'Chraiet';
  static const String lastName = 'Elarbi';

  // Country latitude and longitude
  static const double lat = 48.864716;
  static const double long = 2.349014;

  // weather unique API key
  static const String appid = 'd4cb16a911208da9cad4e70aea2599a7';

  // image base url
  static const String imageBaseUrl = 'http://openweathermap.org/img/wn/';

  /* Default Border radius */
  static const double smallRadiusValue = 4.0;
  static const double regularRadiusValue = 6.0;
  static const double normalRadiusValue = 10.0;
  static const double largeRadiusValue = 20.0;

  static const Radius smallRadius = Radius.circular(smallRadiusValue);
  static const Radius regularRadius = Radius.circular(regularRadiusValue);
  static const Radius normalRadius = Radius.circular(normalRadiusValue);
  static const Radius largeRadius = Radius.circular(largeRadiusValue);

  static const BorderRadius smallBorderRadius = BorderRadius.all(smallRadius);
  static const BorderRadius regularBorderRadius = BorderRadius.all(regularRadius);
  static const BorderRadius normalBorderRadius = BorderRadius.all(normalRadius);
  static const BorderRadius largeBorderRadius = BorderRadius.all(largeRadius);
}
