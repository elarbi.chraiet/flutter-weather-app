enum Environment { dev, staging, prod }

class AppEnvironments {
  static Environment environment = Environment.dev;

  static String get baseUrl {
    switch (environment) {
      case Environment.staging:
        return 'https://api.openweathermap.org/data/2.5/';

      case Environment.prod:
        return 'https://api.openweathermap.org/data/2.5/';

      default:
        return 'https://api.openweathermap.org/data/2.5/';
    }
  }
}
