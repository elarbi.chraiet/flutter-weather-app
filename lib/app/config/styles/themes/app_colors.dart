import 'package:flutter/material.dart';

class AppColors {
  AppColors._();

  /// Light
  static Color primaryColorLight = const Color(0xFF5D97A9);
  static Color secondaryColorLight = const Color(0x60d9f4fc);
  static Color backgroundLight = const Color(0xFFE6E4E8);
  static Color whiteLight = const Color(0xFFFFFFFF);
  static Color blackLight = const Color(0xFF353B3F);
  static Color greyLight = const Color(0xFFF4F4F5);
  static Color titleLight = const Color(0xFF012F2D);
  static Color textLight = const Color(0xFF465453);
  static Color borderLight = const Color(0xFFC6C9DB);
  static Color errorLight = const Color(0xFFFC5656);

  /// Dark
  static Color primaryColorDark = const Color(0xFF5D97A9);
  static Color secondaryColorDark = const Color(0xFF5D97A9);
  static Color backgroundDark = const Color(0xFF272D31);
  static Color whiteDark = const Color(0xFF353B3F);
  static Color blackDark = const Color(0xFFFFFFFF);
  static Color greyDark = const Color(0xFF373E43);
  static Color titleDark = const Color(0xFFFFFFFE);
  static Color textDark = const Color(0xFFBFCACA);
  static Color borderDark = const Color(0x1AA8ABAA);
  static Color errorDark = const Color(0xFFFC5656);
}
