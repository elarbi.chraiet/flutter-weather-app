import 'package:flutter/material.dart';
import 'index.dart';

class AppThemes {
  AppThemes._();

  static ThemeData? darkTheme = ThemeData(
    primaryColor: AppColors.primaryColorDark,
    backgroundColor: AppColors.backgroundDark,
    textTheme: AppTextTheme.textThemeDark,
    iconTheme: IconThemeData(color: AppColors.textDark),
    elevatedButtonTheme: AppButtonTheme.elevatedButtonThemeDataDark,
    inputDecorationTheme: AppTextTheme.inputDecorationThemeDark,
    cardColor: AppColors.greyDark,
    brightness: Brightness.dark,
    appBarTheme: AppBarTheme(
      backgroundColor: AppColors.primaryColorDark,
    ),
  );

  static ThemeData? lightTheme = ThemeData(
    primaryColor: AppColors.primaryColorLight,
    backgroundColor: AppColors.backgroundLight,
    textTheme: AppTextTheme.textThemeLight,
    iconTheme: IconThemeData(color: AppColors.textLight),
    elevatedButtonTheme: AppButtonTheme.elevatedButtonThemeDataLight,
    inputDecorationTheme: AppTextTheme.inputDecorationThemeLight,
    cardColor: AppColors.secondaryColorLight,
    brightness: Brightness.light,
    appBarTheme: AppBarTheme(
      backgroundColor: AppColors.primaryColorLight,
    ),
  );
}
