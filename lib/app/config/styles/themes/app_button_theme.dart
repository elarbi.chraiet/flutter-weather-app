import 'package:flutter/material.dart';
import 'package:weather_test/app/config/constants/app_constants.dart';
import 'index.dart';

class AppButtonTheme {
  AppButtonTheme._();

  /// ElevatedButton
  static final ElevatedButtonThemeData elevatedButtonThemeDataLight = ElevatedButtonThemeData(
    style: _buttonStyleLight,
  );

  static final ElevatedButtonThemeData elevatedButtonThemeDataDark = ElevatedButtonThemeData(
    style: _buttonStyleDark,
  );

  static final ButtonStyle _buttonStyleLight = ButtonStyle(
    fixedSize: MaterialStateProperty.resolveWith<Size>(
      (Set<MaterialState> states) => const Size(
        double.maxFinite,
        48,
      ),
    ),
    backgroundColor: MaterialStateProperty.resolveWith<Color>((states) {
      return AppColors.primaryColorLight;
    }),
    textStyle: MaterialStateProperty.resolveWith<TextStyle>((states) {
      return TextStyle(
        fontFamily: 'Montserrat',
        color: AppColors.whiteLight,
        fontWeight: FontWeight.w500,
        fontSize: 14,
      );
    }),
    elevation: MaterialStateProperty.resolveWith<double>(
      (Set<MaterialState> states) => 2,
    ),
    shape: MaterialStateProperty.resolveWith<OutlinedBorder>(
      (states) => const RoundedRectangleBorder(
        borderRadius: AppConstants.smallBorderRadius,
      ),
    ),
  );

  static final ButtonStyle _buttonStyleDark = ButtonStyle(
    fixedSize: MaterialStateProperty.resolveWith<Size>(
      (Set<MaterialState> states) => const Size(
        double.maxFinite,
        48,
      ),
    ),
    backgroundColor: MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states) {
      return AppColors.primaryColorDark;
    }),
    textStyle: MaterialStateProperty.resolveWith<TextStyle>((states) {
      return TextStyle(
        fontFamily: 'Montserrat',
        color: AppColors.whiteLight,
        fontWeight: FontWeight.w500,
        fontSize: 14,
      );
    }),
    elevation: MaterialStateProperty.resolveWith<double>(
      (Set<MaterialState> states) => 2,
    ),
    shape: MaterialStateProperty.resolveWith<OutlinedBorder>(
      (states) => const RoundedRectangleBorder(
        borderRadius: AppConstants.smallBorderRadius,
      ),
    ),
  );
}
