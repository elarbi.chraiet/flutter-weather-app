import 'package:flutter/material.dart';
import 'package:weather_test/app/config/styles/themes/app_colors.dart';

class AppTextTheme {
  AppTextTheme._();

  static const _fontFamily = 'Montserrat';

  static final TextTheme textThemeLight = TextTheme(
    headline1: _headline1Light,
    headline2: _headline2Light,
    headline3: _headline3Light,
    headline4: _headline4Light,
    headline5: _headline5Light,
    subtitle2: _subTitle2Light,
    bodyText1: _bodyText1Light,
    bodyText2: _bodyText2Light,
    button: _buttonLight,
  );

  static final TextStyle _headline1Light = TextStyle(
    fontWeight: FontWeight.w700,
    fontSize: 36,
    height: 1,
    color: AppColors.titleLight,
    fontFamily: _fontFamily,
  );

  static final TextStyle _headline2Light = TextStyle(
    fontWeight: FontWeight.w600,
    fontSize: 28,
    height: 1,
    color: AppColors.secondaryColorLight,
    fontFamily: _fontFamily,
  );

  static final TextStyle _headline3Light = TextStyle(
    fontWeight: FontWeight.w600,
    fontSize: 16,
    height: 1,
    color: AppColors.titleLight,
    fontFamily: _fontFamily,
  );

  static final TextStyle _headline4Light = TextStyle(
    fontSize: 18,
    height: 1,
    color: AppColors.textLight,
    fontFamily: _fontFamily,
  );

  static final TextStyle _headline5Light = TextStyle(
    fontSize: 16,
    height: 1,
    color: AppColors.primaryColorLight,
    fontFamily: _fontFamily,
  );

  static final TextStyle _subTitle2Light = TextStyle(
    fontSize: 14,
    height: 1,
    color: AppColors.errorDark,
    fontFamily: _fontFamily,
  );

  static final TextStyle _buttonLight = TextStyle(
    fontWeight: FontWeight.w600,
    fontSize: 14,
    height: 1,
    color: AppColors.whiteLight,
    fontFamily: _fontFamily,
  );

  static final TextStyle _bodyText1Light = TextStyle(
    fontSize: 14,
    height: 1,
    color: AppColors.titleLight,
    fontFamily: _fontFamily,
  );

  static final TextStyle _bodyText2Light = TextStyle(
    fontWeight: FontWeight.w600,
    fontSize: 12,
    height: 1,
    color: AppColors.borderLight,
    fontFamily: _fontFamily,
  );

  static final TextTheme textThemeDark = TextTheme(
    headline1: _headline1Dark,
    headline2: _headline2Dark,
    headline3: _headline3Dark,
    headline4: _headline4Dark,
    headline5: _headline5Dark,
    subtitle2: _subTitle2Dark,
    bodyText1: _bodyText1Dark,
    bodyText2: _bodyText2Dark,
    button: _buttonDark,
  );

  static final TextStyle _headline1Dark = _headline1Light.copyWith(color: AppColors.titleDark);
  static final TextStyle _headline2Dark = _headline2Light.copyWith(color: AppColors.secondaryColorDark);

  static final TextStyle _headline3Dark = _headline3Light.copyWith(color: AppColors.titleDark);
  static final TextStyle _headline4Dark = _headline4Light.copyWith(color: AppColors.textDark);
  static final TextStyle _headline5Dark = _headline5Light.copyWith(color: AppColors.primaryColorDark);

  static final TextStyle _subTitle2Dark = _subTitle2Light.copyWith(color: AppColors.errorDark);
  static final TextStyle _buttonDark = _buttonLight.copyWith(color: AppColors.whiteDark);
  static final TextStyle _bodyText1Dark = _bodyText1Light.copyWith(color: AppColors.titleDark);
  static final TextStyle _bodyText2Dark = _bodyText2Light.copyWith(color: AppColors.titleDark);

  static final InputDecorationTheme inputDecorationThemeLight = InputDecorationTheme(
    fillColor: AppColors.whiteLight,
    filled: true,
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(color: AppColors.borderLight),
    ),
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(color: AppColors.primaryColorLight),
    ),
    errorBorder: OutlineInputBorder(
      borderSide: BorderSide(color: AppColors.errorLight),
    ),
    focusedErrorBorder: OutlineInputBorder(
      borderSide: BorderSide(color: AppColors.errorLight),
    ),
    disabledBorder: const OutlineInputBorder(
      borderSide: BorderSide(color: Colors.transparent),
    ),
  );

  static final InputDecorationTheme inputDecorationThemeDark = InputDecorationTheme(
    filled: true,
    fillColor: AppColors.greyDark,
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(color: AppColors.textDark),
    ),
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(color: AppColors.primaryColorDark),
    ),
    errorBorder: OutlineInputBorder(
      borderSide: BorderSide(color: AppColors.errorDark),
    ),
    focusedErrorBorder: OutlineInputBorder(
      borderSide: BorderSide(color: AppColors.errorDark),
    ),
    disabledBorder: const OutlineInputBorder(
      borderSide: BorderSide(color: Colors.transparent),
    ),
  );
}
