class AppMargins {
  AppMargins._();

  static const double margin6 = 6;
  static const double margin8 = 8;
  static const double margin10 = 10;
  static const double margin12 = 12;
  static const double margin20 = 20;
  static const double margin32 = 32;
  static const double margin72 = 72;
}
