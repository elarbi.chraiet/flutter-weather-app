import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:weather_test/app/config/routes/routing.dart';
import 'package:weather_test/app/config/styles/app_margins.dart';

import '../../utils/index.dart';
import '../../widgets/index.dart';
import '../config/constants/index.dart';
import '../config/styles/themes/index.dart';
import '../controllers/index.dart';

enum LoginPageKeys {
  emailInputKey,
  passwordInputKey,
  loginButtonKey,
}

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final LoginPageController _loginController = Modular.get<LoginPageController>();

  final GlobalKey<FormState> _loginFormKey = GlobalKey<FormState>();
  bool _isObscure = true;

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<bool?>(
      valueListenable: _loginController.isLoading,
      builder: (context, box, widget) {
        return AbsorbPointer(
          absorbing: _loginController.isLoading.value ?? false,
          child: GestureDetector(
            onTap: () {
              UsefullMethods.unfocus(
                context,
              );
            },
            child: Scaffold(
              body: SafeArea(
                child: Form(
                  key: _loginFormKey,
                  child: SingleChildScrollView(
                    padding: const EdgeInsets.only(
                      top: AppMargins.margin72,
                      left: AppMargins.margin32,
                      right: AppMargins.margin32,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          AppStrings.loginPageTitle,
                          style: Theme.of(context).textTheme.headline5?.copyWith(
                                height: 1.2,
                                fontSize: 32,
                                fontWeight: FontWeight.bold,
                              ),
                        ),
                        const VerticalSpacing(
                          AppMargins.margin72,
                        ),
                        InputTextField(
                          key: keyFromEnumValue(
                            LoginPageKeys.emailInputKey,
                          ),
                          labelText: AppStrings.emailTextFieldLabel,
                          controller: _loginController.emailTextEditingController,
                          focusNode: _loginController.emailFocusNode,
                          nextFocusNode: _loginController.passwordFocusNode,
                          keyboardType: TextInputType.emailAddress,
                          inputType: InputType.alphaNumeric,
                          textInputAction: TextInputAction.next,
                          validator: Validators.validateEmail,
                        ),
                        const VerticalSpacing(
                          AppMargins.margin20,
                        ),
                        InputTextField(
                          key: keyFromEnumValue(
                            LoginPageKeys.passwordInputKey,
                          ),
                          labelText: AppStrings.passwordTextFieldLabel,
                          controller: _loginController.passwordTextEditingController,
                          focusNode: _loginController.passwordFocusNode,
                          obscureText: _isObscure,
                          inputType: InputType.alphaNumeric,
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.previous,
                          validator: Validators.validatePassword,
                          suffixIcon: IconButton(
                            splashColor: Colors.transparent,
                            icon: Icon(
                              _isObscure ? Icons.visibility : Icons.visibility_off,
                              color: Theme.of(context).primaryColor,
                              size: 20,
                            ),
                            onPressed: () {
                              setState(() {
                                _isObscure = !_isObscure;
                              });
                            },
                          ),
                        ),
                        const VerticalSpacing(
                          AppMargins.margin32,
                        ),
                        ElevatedButtonWidget(
                          key: keyFromEnumValue(
                            LoginPageKeys.loginButtonKey,
                          ),
                          onPressed: () {
                            _login();
                          },
                          loader: _loginController.isLoading.value ?? false,
                          label: AppStrings.loginButtonLabel,
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  void _login() {
    UsefullMethods.unfocus(
      context,
    );
    final bool? validInputs = _loginFormKey.currentState?.validate();
    if (validInputs ?? false) {
      _loginController.login().then(
            (bool? result) => {
              if (result ?? false)
                {
                  //Push HomePage
                  Modular.to.pushReplacementNamed(
                    AppRoutes.homePage,
                  )
                }
              else
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: Text(
                      AppStrings.loginErrorMessageText,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: AppColors.whiteLight,
                        fontWeight: FontWeight.w500,
                        fontSize: 14,
                      ),
                    ),
                    backgroundColor: AppColors.errorLight,
                  ),
                )
            },
          );
    }
  }
}
