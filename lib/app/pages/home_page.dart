import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:weather_test/app/config/constants/index.dart';
import 'package:weather_test/app/config/routes/app_routes.dart';
import 'package:weather_test/app/config/styles/app_margins.dart';
import 'package:weather_test/app/controllers/index.dart';
import 'package:weather_test/data/models/index.dart';
import 'package:weather_test/utils/index.dart';
import '../../widgets/index.dart';
import '../config/styles/themes/index.dart';

enum HomePageKeys {
  userNameKey,
  logoutButtonKey,
  confirmLogoutButtonKey,
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final HomePageController _homeController = Modular.get<HomePageController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: [
            const Icon(
              CupertinoIcons.person_alt_circle,
              size: 40,
            ),
            const HorizontalSpacing(
              AppMargins.margin8,
            ),
            Expanded(
              child: Text(
                key: keyFromEnumValue(
                  HomePageKeys.userNameKey,
                ),
                '${HiveUtils.getLoggedUserBox()?.firstName ?? ''} ${HiveUtils.getLoggedUserBox()?.lastName ?? ''}',
              ),
            ),
          ],
        ),
        actions: [
          Center(
            child: TextButton(
              key: keyFromEnumValue(
                HomePageKeys.logoutButtonKey,
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: AppMargins.margin8,
                ),
                child: Text(
                  AppStrings.logoutLabelText,
                  style: Theme.of(context).textTheme.subtitle2?.copyWith(
                        fontSize: 13,
                        fontWeight: FontWeight.w700,
                        decoration: TextDecoration.underline,
                      ),
                ),
              ),
              onPressed: () {
                _showDeconnexionAlertDialog();
              },
            ),
          ),
        ],
      ),
      body: FutureBuilder<List<Weatherinfos>?>(
        future: _homeController.getWeatherDetails(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: LoadingWidget(
                color: AppColors.primaryColorLight,
                size: 50.0,
              ),
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            var weathersList = snapshot.data;
            if (weathersList != null) {
              return RefreshIndicator(
                onRefresh: _refreshWeatherList,
                color: AppColors.primaryColorLight,
                child: ListView.builder(
                  padding: const EdgeInsets.symmetric(
                    vertical: AppMargins.margin20,
                  ),
                  shrinkWrap: true,
                  physics: const BouncingScrollPhysics(),
                  itemCount: weathersList.length,
                  itemBuilder: (_, final int index) {
                    var weatherElement = weathersList[index];
                    return WeatherItemWidget(
                      dateTime: weatherElement.dtTxt,
                      weatherValue: weatherElement.main?.temp,
                      weatherDescription: weatherElement.weather?[0].description,
                      weatherIconName: weatherElement.weather?[0].icon,
                    );
                  },
                ),
              );
            } else {
              return Center(
                child: GestureDetector(
                  onTap: _refreshWeatherList,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        AppStrings.genericErrorDescriptionText,
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.headline3?.copyWith(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                            ),
                      ),
                      const VerticalSpacing(
                        AppMargins.margin10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            AppStrings.tryLaterText,
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline3?.copyWith(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                ),
                          ),
                          const Icon(
                            Icons.refresh,
                          )
                        ],
                      )
                    ],
                  ),
                ),
              );
            }
          }
          return const SizedBox();
        },
      ),
    );
  }

  void _showDeconnexionAlertDialog() {
    showDialog<void>(
      barrierDismissible: false,
      context: context,
      builder: (context) => AlertDialogWidget(
        title: AppStrings.logoutLabelText,
        content: AppStrings.logoutDescriptionText,
        buttonLabel: AppStrings.cancelLabelText,
        function: () {
          Modular.to.pop();
        },
        secondButtonLabel: AppStrings.confirmLabelText,
        secondFunction: () {
          _logout();
        },
      ),
    );
  }

  Future<void> _refreshWeatherList() async {
    _homeController.getWeatherDetails();
    setState(() {});
  }

  Future<void> _logout() async {
    Modular.to.pop();
    await HiveUtils.clearLoggedUserBox();
    Modular.to.pushReplacementNamed(
      AppRoutes.loginPage,
    );
  }
}
