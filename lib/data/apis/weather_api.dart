import 'package:weather_test/data/interceptors/base_dio_interceptor.dart';
import 'package:weather_test/data/models/index.dart';
import '../../app/config/constants/index.dart';

class WeatherApi {
  BaseDioInterceptor interceptor = BaseDioInterceptor();

  /// Get weather details api
  Future<FindWeatherResponse?> findWeatherInfos() async {
    return BaseDioInterceptor()
        .dio
        ?.get(
          'forecast?lat=${AppConstants.lat}&lon=${AppConstants.long}&appid=${AppConstants.appid}&lang=fr&units=metric',
        )
        .then((response) {
      if (response.data == null || interceptor.getJson(response.data) == null) {
        return null;
      }
      return FindWeatherResponse.fromJson(interceptor.getJson(response.data)!);
    });
  }
}
