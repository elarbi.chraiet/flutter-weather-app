import 'package:flutter_modular/flutter_modular.dart';
import 'package:weather_test/data/apis/weather_api.dart';
import 'package:weather_test/data/models/index.dart';

class WeatherRepository {
  final WeatherApi _weatherApi = Modular.get<WeatherApi>();

  /// Find weather details
  Future<FindWeatherResponse?> findWeatherInfos() {
    return _weatherApi.findWeatherInfos();
  }
}
