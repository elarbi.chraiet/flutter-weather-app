import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:weather_test/app/config/env/app_environnments.dart';

import '../../utils/index.dart';

class BaseDioInterceptor {
  Dio? _dio;

  //! Interceptor Setup
  Dio? get dio {
    if (_dio == null) {
      _dio = Dio(
        BaseOptions(
          baseUrl: AppEnvironments.baseUrl,
          connectTimeout: 50000,
          sendTimeout: 50000,
          receiveTimeout: 50000,
        ),
      );

      _dio?.interceptors.add(
        InterceptorsWrapper(
          onRequest: (
            RequestOptions requestOptions,
            RequestInterceptorHandler handler,
          ) {
            _describeAndHandleRequestResult(requestOptions: requestOptions);
            handler.next(requestOptions);
          },
          onResponse: (
            response,
            ResponseInterceptorHandler handler,
          ) {
            _describeAndHandleRequestResult(response: response);
            handler.next(response);
          },
          onError: (
            DioError error,
            ErrorInterceptorHandler handler,
          ) {
            _describeAndHandleRequestResult(error: error);
            handler.next(error);
          },
        ),
      );
    }

    return _dio;
  }

  //! Error Handling

  //! HELPERS
  /// should be called after every response/failure to let the interceptor
  /// work correctly as almost all the APIs are of type 'application/json'
  void _describeAndHandleRequestResult({
    final RequestOptions? requestOptions,
    final Response<dynamic>? response,
    final DioError? error,
  }) {
    if (requestOptions != null) {
      Print.yellow(
          '___________________________________________________________________________________________________________________');
      Print.yellow('|    ---- Endpoint        : ${requestOptions.path}');
      Print.yellow('|    ---- headers         : ${requestOptions.headers}');
      Print.yellow('|    ---- queryParameters : ${requestOptions.queryParameters}');
      Print.yellow('|    ---- data            : ${requestOptions.data}');
      Print.yellow(
          '___________________________________________________________________________________________________________________');
    } else {
      if (response != null) {
        Print.green(
            '___________________________________________________________________________________________________________________');
        Print.green('|    ---- Endpoint        : ${response.requestOptions.path}');
        Print.green('|    ---- Status Code:    : ${response.statusCode}');
        Print.green('|    ---- Status Message  : ${response.statusMessage}');
        Print.green('|    ---- Data            : ${response.data}');
        Print.green('___________________________________________________________________________________');
      } else if (error != null) {
        Print.red(
            '___________________________________________________________________________________________________________________');
        Print.red('|    ---- Endpoint       : ${error.requestOptions.path}');
        Print.red('|    ---- Error Type     : ${error.type}');
        Print.red('|    ---- Status Message : ${error.response?.statusMessage}');
        Print.red('|    ---- Status code    : ${error.response?.statusCode}');
        Print.red('|    ---- Error message  : ${error.response?.data}');
        Print.red(
            '___________________________________________________________________________________________________________________');
      }
    }
  }

  /// #### Converts string response to a map
  Map<String, dynamic>? getJson(final data) {
    return (data is String) ? jsonDecode(data) : data as Map<String, dynamic>?;
  }
}
