import 'package:hive/hive.dart';
part 'logged_user.g.dart';

@HiveType(typeId: 0)
class LoggedUser {
  LoggedUser({
    this.email,
    this.firstName,
    this.lastName,
  });

  @HiveField(0)
  String? email;

  @HiveField(1)
  String? firstName;

  @HiveField(2)
  String? lastName;
}
