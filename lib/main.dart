import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:weather_test/app_widget.dart';
import 'package:weather_test/utils/index.dart';
import 'app/config/env/app_environnments.dart';
import 'app/config/routes/routing.dart';

Future<void> main() async {
  // Setup hive
  await HiveUtils.launchHive();
  setupEnvironnment(Environment.dev);
  runApp(
    ModularApp(
      module: AppModule(),
      child: const AppWidget(),
    ),
  );
}

// Change environment
void setupEnvironnment(Environment environment) {
  AppEnvironments.environment = environment;
}
