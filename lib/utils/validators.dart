import 'package:weather_test/app/config/constants/app_strings.dart';

class Validators {
  Validators._();

  static String? validateEmail(final String? value) {
    if (value == null || value.isEmpty) return AppStrings.requiredEmailErrorText;
    final regExp = RegExp(
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');

    if (!regExp.hasMatch(value)) {
      return AppStrings.invalidEmailErrorText;
    } else {
      return null;
    }
  }

  static String? validatePassword(final String? password) {
    if (password == null || password.isEmpty) {
      return AppStrings.requiredPasswordErrorText;
    }
    if (password.length < 8) {
      return AppStrings.invalidPasswordErrorText;
    }
    return null;
  }
}
