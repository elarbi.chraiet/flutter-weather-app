import 'package:flutter/material.dart';

class UsefullMethods {
  UsefullMethods._();

  static void unfocus(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
  }
}
