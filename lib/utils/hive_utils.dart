import 'package:flutter/foundation.dart';
import 'package:hive_flutter/hive_flutter.dart';

import '../data/models/index.dart';

class HiveUtils {
  HiveUtils._();

  static Future<void> launchHive() async {
    await Hive.initFlutter();
    if (!Hive.isBoxOpen(_loggedUserBoxKey)) {
      Hive.registerAdapter<LoggedUser>(LoggedUserAdapter());
      await Hive.openBox<LoggedUser>(_loggedUserBoxKey);
    }
  }

  // ======================================================================================================
  // ========================================== LOGGED USER ===============================================
  // ======================================================================================================
  static const String _loggedUserBoxKey = 'loggedUserBoxKey';

  static ValueListenable<Box<LoggedUser>> loggedUserBoxValueListenable =
      Hive.box<LoggedUser>(_loggedUserBoxKey).listenable();

  static final Box<LoggedUser> _loggedUserBox = loggedUserBoxValueListenable.value;

  static Future<void> saveLoggedUser({
    String? email,
    String? firstName,
    String? lastName,
  }) async {
    _loggedUserBox.put(
        _loggedUserBoxKey,
        LoggedUser(
          email: email,
          firstName: firstName,
          lastName: lastName,
        ));
  }

  static LoggedUser? getLoggedUserBox() {
    LoggedUser? loggedUser = _loggedUserBox.get(_loggedUserBoxKey);
    return loggedUser;
  }

  static Future<void> clearLoggedUserBox() async {
    _loggedUserBox.clear();
  }
}
