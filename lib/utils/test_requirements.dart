import 'dart:convert';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

extension StringMocking on String {
  Future<String> get loadJsonMockFileForFullTestProcess async => await rootBundle.loadString('test/json/$this');

  Future<Map<String, dynamic>> get toMapForFullTestProcess async =>
      jsonDecode(await loadJsonMockFileForFullTestProcess);

  String get loadJsonMockFile => File('test/json/$this').readAsStringSync();

  Map<String, dynamic> get toMap => jsonDecode(loadJsonMockFile);
}

Key keyFromEnumValue(keyEnum) => Key(keyEnum.toString());
