import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:weather_test/app/config/styles/app_margins.dart';

import '../app/config/styles/themes/index.dart';

enum InputType { numeric, alphabetic, alphaNumeric }

class InputTextField extends StatelessWidget {
  const InputTextField(
      {Key? key,
      this.autofocus = false,
      this.obscureText = false,
      required this.controller,
      this.hintText,
      this.labelText,
      this.onChanged,
      this.onSaved,
      this.onEditingComplete,
      this.onFieldSubmitted,
      this.onTap,
      this.maxLines = 1,
      this.inputType,
      this.focusNode,
      this.nextFocusNode,
      this.keyboardType = TextInputType.multiline,
      this.validator,
      this.fillColor = Colors.white,
      this.filled = true,
      this.suffixIcon,
      this.prefixIcon,
      this.title,
      this.textInputAction = TextInputAction.next,
      this.inputFormatters,
      this.maxLength,
      this.autovalidateMode,
      this.initialValue,
      this.textCapitalization = TextCapitalization.none,
      this.enabled,
      this.size})
      : super(key: key);

  final bool autofocus;
  final TextEditingController controller;
  final String? hintText;
  final String? labelText;
  final bool? enabled;
  final void Function()? onTap;
  final void Function(String?)? onChanged;
  final void Function(String?)? onSaved;
  final void Function()? onEditingComplete;
  final void Function(String)? onFieldSubmitted;
  final int maxLines;
  final InputType? inputType;
  final FocusNode? focusNode;
  final FocusNode? nextFocusNode;
  final TextInputType keyboardType;
  final String? Function(String?)? validator;
  final bool obscureText;
  final Color fillColor;
  final bool filled;
  final int? maxLength;
  final bool? maxLengthEnforced = true;
  final Widget? suffixIcon;
  final Widget? prefixIcon;
  final String? title;
  final String? initialValue;
  final TextInputAction? textInputAction;
  final AutovalidateMode? autovalidateMode;
  final List<TextInputFormatter>? inputFormatters;
  final TextCapitalization textCapitalization;
  final Size? size;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      key: key,
      autofocus: autofocus,
      obscureText: obscureText,
      controller: controller,
      keyboardType: keyboardType,
      maxLines: maxLines,
      textAlignVertical: TextAlignVertical.center,
      onChanged: onChanged,
      onSaved: onSaved,
      initialValue: initialValue,
      maxLength: maxLength,
      onEditingComplete: onEditingComplete,
      onFieldSubmitted: onFieldSubmitted,
      inputFormatters: inputFormatters,
      style: Theme.of(context).textTheme.bodyText1?.copyWith(
            height: 1.2,
            fontSize: 16,
            fontWeight: FontWeight.w500,
            fontFamily: 'Montserrat',
          ),
      cursorColor: AppColors.primaryColorLight,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.symmetric(
          horizontal: AppMargins.margin12,
          vertical: AppMargins.margin10,
        ),
        errorMaxLines: 3,
        counterText: '',
        hintText: hintText,
        labelText: labelText,
        labelStyle: Theme.of(context).textTheme.bodyText2?.copyWith(
              height: 1.2,
              fontWeight: FontWeight.w500,
              fontSize: 14,
            ),
        suffixIcon: suffixIcon,
        prefixIcon: prefixIcon,
      ),
      autocorrect: false,
      enableSuggestions: false,
      enabled: enabled,
      textInputAction: textInputAction,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      validator: validator,
      enableInteractiveSelection: true,
      focusNode: focusNode,
      textCapitalization: textCapitalization,
    );
  }
}
