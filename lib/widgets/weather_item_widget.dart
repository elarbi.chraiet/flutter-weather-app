import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:weather_test/app/config/constants/index.dart';
import 'package:weather_test/app/config/styles/themes/app_colors.dart';
import 'package:weather_test/widgets/index.dart';
import '../app/config/styles/app_margins.dart';

class WeatherItemWidget extends StatelessWidget {
  const WeatherItemWidget({
    this.dateTime,
    this.weatherValue,
    this.weatherDescription,
    this.weatherIconName,
    Key? key,
  }) : super(key: key);

  final DateTime? dateTime;
  final double? weatherValue;
  final String? weatherDescription;
  final String? weatherIconName;

  @override
  Widget build(BuildContext context) {
    final DateFormat dateFormat = DateFormat("dd-MM-yyyy HH:mm");

    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: AppMargins.margin12,
        vertical: AppMargins.margin6,
      ),
      child: Card(
        shape: RoundedRectangleBorder(
          side: BorderSide(
            color: AppColors.primaryColorLight,
          ),
          borderRadius: AppConstants.normalBorderRadius, //<-- SEE HERE
        ),
        elevation: 3,
        child: Padding(
          padding: const EdgeInsets.only(
            left: AppMargins.margin10,
            right: AppMargins.margin10,
            bottom: AppMargins.margin12,
            top: AppMargins.margin12,
          ),
          child: ListTile(
            leading: weatherIconName != null
                ? Image.network(
                    '${AppConstants.imageBaseUrl}$weatherIconName.png',
                  )
                : const SizedBox(),
            title: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(dateTime != null ? dateFormat.format(dateTime!) : '',
                    style: Theme.of(context).textTheme.headline3?.copyWith(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        ),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2),
                const VerticalSpacing(
                  AppMargins.margin10,
                ),
                Text(
                  weatherValue != null ? '${weatherValue?.toInt()} °C' : '',
                  style: Theme.of(context).textTheme.headline5?.copyWith(
                        fontSize: 32,
                        fontWeight: FontWeight.w600,
                      ),
                ),
                const VerticalSpacing(
                  AppMargins.margin10,
                ),
                Text(weatherDescription ?? '',
                    style: Theme.of(context).textTheme.headline4?.copyWith(
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                        ),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
