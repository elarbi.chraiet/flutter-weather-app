import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:weather_test/app/pages/index.dart';
import 'package:weather_test/utils/test_requirements.dart';

class AlertDialogWidget extends StatelessWidget {
  const AlertDialogWidget({
    this.buttonLabel,
    this.secondButtonLabel,
    this.title,
    this.content,
    this.function,
    this.secondFunction,
    Key? key,
  }) : super(key: key);

  final String? title;
  final String? buttonLabel;
  final String? secondButtonLabel;
  final String? content;
  final VoidCallback? function;
  final VoidCallback? secondFunction;

  @override
  Widget build(final BuildContext context) {
    final platform = Theme.of(context).platform;
    if (platform == TargetPlatform.iOS || platform == TargetPlatform.macOS) {
      return CupertinoAlertDialog(
        title: Text(
          title ?? '',
          style: Theme.of(context).textTheme.headline3?.copyWith(
                fontSize: 18,
                fontWeight: FontWeight.w700,
              ),
        ),
        content: Text(
          content ?? '',
          style: Theme.of(context).textTheme.headline4?.copyWith(
                fontSize: 13,
              ),
        ),
        actions: [
          CupertinoDialogAction(
            child: Text(
              buttonLabel ?? '',
              style: Theme.of(context).textTheme.headline5?.copyWith(
                    fontSize: 12,
                    fontWeight: FontWeight.w600,
                  ),
            ),
            onPressed: () => function != null ? function!.call() : () {},
          ),
          CupertinoDialogAction(
            key: keyFromEnumValue(
              HomePageKeys.confirmLogoutButtonKey,
            ),
            child: Text(
              secondButtonLabel ?? '',
              style: Theme.of(context).textTheme.headline5?.copyWith(
                    fontSize: 12,
                    fontWeight: FontWeight.w600,
                  ),
            ),
            onPressed: () => secondFunction != null ? secondFunction!.call() : () {},
          ),
        ],
      );
    } else {
      return WillPopScope(
        onWillPop: () async => false,
        child: AlertDialog(
          title: Text(
            title ?? '',
            style: Theme.of(context).textTheme.headline3?.copyWith(
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                ),
          ),
          content: Text(
            content ?? '',
            style: Theme.of(context).textTheme.headline4?.copyWith(
                  fontSize: 13,
                ),
          ),
          actions: [
            TextButton(
              onPressed: () => function != null ? function!.call() : () {},
              child: Text(
                buttonLabel ?? '',
                style: Theme.of(context).textTheme.headline5?.copyWith(
                      fontSize: 12,
                      fontWeight: FontWeight.w600,
                    ),
              ),
            ),
            TextButton(
              key: keyFromEnumValue(
                HomePageKeys.confirmLogoutButtonKey,
              ),
              onPressed: () => secondFunction != null ? secondFunction!.call() : () {},
              child: Text(
                secondButtonLabel ?? '',
                style: Theme.of(context).textTheme.headline5?.copyWith(
                      fontSize: 12,
                      fontWeight: FontWeight.w600,
                    ),
              ),
            ),
          ],
        ),
      );
    }
  }
}
