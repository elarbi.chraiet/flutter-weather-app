import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoadingWidget extends StatelessWidget {
  const LoadingWidget({
    Key? key,
    this.size,
    required this.color,
  }) : super(key: key);

  final double? size;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return SpinKitCircle(
      size: size ?? 0,
      color: color,
    );
  }
}
