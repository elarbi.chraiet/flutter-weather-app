import 'package:flutter/material.dart';
import 'package:weather_test/app/config/styles/themes/app_colors.dart';
import 'package:weather_test/widgets/index.dart';

class ElevatedButtonWidget extends StatelessWidget {
  const ElevatedButtonWidget({
    Key? key,
    this.label,
    this.onPressed,
    this.loader = false,
  }) : super(key: key);

  final Function()? onPressed;
  final String? label;
  final bool loader;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      child: loader
          ? LoadingWidget(
              size: 48,
              color: AppColors.whiteLight,
            )
          : Text(
              label ?? '',
            ),
    );
  }
}
